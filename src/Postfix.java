import java.util.Stack;

public class Postfix {
    public static String infixToPostfix(String infix) {
        StringBuilder postfix = new StringBuilder();
        Stack<Character> operatorStack = new Stack<>();

        for (char c : infix.toCharArray()) {
            if (Character.isDigit(c) || Character.isLetter(c)) {
                postfix.append(c); // Operand, add to postfix
            } else if (c == '(') {
                operatorStack.push(c); // Push '(' onto the stack
            } else if (c == ')') {
                // Pop operators and append to postfix until '(' is encountered
                while (!operatorStack.isEmpty() && operatorStack.peek() != '(') {
                    postfix.append(operatorStack.pop());
                }
                if (!operatorStack.isEmpty() && operatorStack.peek() == '(') {
                    operatorStack.pop(); // Pop and discard '('
                }
            } else {
                // Operator encountered
                while (!operatorStack.isEmpty() && precedence(c) <= precedence(operatorStack.peek())) {
                    postfix.append(operatorStack.pop());
                }
                operatorStack.push(c);
            }
        }

        // Pop any remaining operators from the stack and append to postfix
        while (!operatorStack.isEmpty()) {
            postfix.append(operatorStack.pop());
        }

        return postfix.toString();
    }

    private static int precedence(char operator) {
        switch (operator) {
            case '+':
            case '-':
                return 1;
            case '*':
            case '/':
                return 2;
            default:
                return 0; // Other characters (operands, parentheses, etc.)
        }
    }
}
